<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';
require 'phpmailer/src/Exception.php';

if (array_key_exists('email', $_POST)) {

    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $name = htmlspecialchars($_POST['name']);
    $phone = htmlspecialchars($_POST['contact']);
    $website = htmlspecialchars($_POST['website']);
    $country = htmlspecialchars($_POST['country']);
    $address = htmlspecialchars($_POST['address']);
    $type_of_company = htmlspecialchars($_POST['type_of_company']);
    $person_name = htmlspecialchars($_POST['person_name']);
    $person_contact = htmlspecialchars($_POST['person_contact']);
    $person_email = htmlspecialchars($_POST['person_email']);
    $version = htmlspecialchars($_POST['version']);

    $type = 'Registeration';

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = 'mail.d5n.in';
    $mail->Port = 587;
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->Username = 'mail@d5n.in';
    $mail->Password = 'jMF75d*Q~[{S';
    $mail->setFrom('mail@d5n.in');
    $mail->addAddress('info@searveeazy.com');

 
    $mail->Subject = $type;

    $mail->isHTML(false);
    $mail->Body = <<<EOT
       Company Details :- 
       --------------------------
        Name of Comapny: $name
        Email: $email
        Contact: $phone
        Website: $website
        Country: $country
        Address: $address
        Type of Company: $type_of_company
        Version : $version

       contact Person Details :-
       --------------------------
        Name: $person_name
        Contact: $person_contact
         Email : $person_email
      
EOT;

    try {
        $mail->send();
        echo "<script> alert('Email has been sent successfully');
        </script>";
        echo "<script> 
            window.location.href = 'index.html';
        </script>";
    } catch (Exception $e) {
        echo "<script> alert('Sorry, the email could not be sent. Please try again later. Error: " . $e->getMessage() . "');
        </script>";
    }
}
?>
